#ifndef STRUCTURES_LIST_H
#define STRUCTURES_LIST_H
#include"Node.h"

/**
 * Representa uma lista.
 *
 * @tparam T o tipo do item da lista.
 */
template <typename T>
class List {
private:
    Node<T> * _start = NULL;
    int _index = 0;
public:

    /**
     * Insere um elemento ao final da lista.
     *
     * @param item o elemento a ser inserido.
     */
    void append(T item)
    {
        if(_index == 0)
        {
            _start = new Node<T>(item);
        }
        else
        {
            Node<T> *last = _start;
            while(last->getNext()!=NULL)
            {
                last=last->getNext();
            }
            last->setNext(new Node<T>(item));
        }
        _index++;
    }

    /**
     * Insere um elemento na lista, na posição especificada.
     *
     * @param index o índice do elemento.
     * @param item o item a ser inserido.
     * @throw "Posição inválida" quando o índice estiver fora do alcance.
     */
    void insertAt(int index, T item)
    {
        if(index==(_index+1))
        {
            append(item);
        }
        else if(index>_index)
        {
            throw "Posição inválida!";
        }
        else if(index==1)
        {
            Node<T> *itemAdd = new Node<T>(item);
            Node<T> *seccond = _start;
            itemAdd->setNext(seccond);
            _start = itemAdd;
            _index++;
        }
        else
        {
            Node<T> *itemPosition = _start;
            Node<T> *penult = nullptr;
            Node<T> *itemAdd = new Node<T>(item);
            for(int i = 1; i<index; i++)
            {
                penult = itemPosition;
                itemPosition = itemPosition->getNext();
            }
            penult->setNext(itemAdd);
            itemAdd->setNext(itemPosition);
            _index++;
        }
    }

    /**
     * Remove um elemento data, da posição especificada.
     *
     * @param index o índice do elemento a ser inserido.
     */
    void removeAt(int index)
    {
        if(index>_index)
        {
            throw "Posição inválida!";
        }
        else if(index==1)
        {
            Node<T> *itemPosition = _start->getNext();
            _start = nullptr;
            delete _start;
            _start = itemPosition;
            _index--;
        }
        else
        {
            Node<T> *itemPosition = _start;
            Node<T> *penult = nullptr;
            for(int i = 1; i<index; i++)
            {
                penult = itemPosition;
                itemPosition = itemPosition->getNext();
            }
            penult->setNext(itemPosition->getNext());
            itemPosition->setNext(nullptr);
            delete itemPosition;
            _index--;
        }
    }

    /**
     * Remove todos os elementos da lista.
     */
    void clear()
    {
        delete _start;
        _index = 0;
    }

    /**
     * @return a quantidade de elementos da lista.
     */
    int count()
    {
        return _index;
    }

    /**
     * Obtém o item na posição especificada.
     *
     * @param index o índice do elemento.
     * @return o elemento encontrado.
     * @throw "Posição Inválida" se o índice for inválido.
     */
    T get(int index)
    {
        if(index>_index)
        {
            throw "Posição inválida!";
        }
        else
        {
            Node<T> *itemPosition = _start;
            for(int i = 1; i<index; i++)
            {
                itemPosition = itemPosition->getNext();
            }
            return itemPosition->getValue();
        }
    }

    /**
     * Define o valor do elemento na posição especificada (sobrescreve).
     *
     * @param index o índice do elemento.
     * @param value o valor do elemento.
     * @throw "Posição Inválida" se o índice for inválido.
     */
    void set(int index, T value)
    {
        if(index>_index)
        {
            throw "Posição inválida!";
        }
        else
        {
            Node<T> *itemPosition = _start;
            for(int i = 1; i<index; i++)
            {
                itemPosition = itemPosition->getNext();
            }
            itemPosition->setValue(value);
        }
    }
    int getIndex() const
    {
        return _index;
    }
};

#endif //STRUCTURES_LIST_H
