#include <iostream>
#include "NumericPyramid.h"
using namespace std;

int main()
{
    int height = 0;

    do
    {
        cout<<"Altura da pir"<<char(131)<<"mide: ";
        cin>>height;

        if(height == 0)
        {
            cout<<"Altura deve ser maior que 0!"<<endl;
        }

    }while(height == 0);

    NumericPyramid p(height);       //cria a pirâmide com a quantidade de linhas dada

    for(int x = 0; x<=height; x++)  //recebe o valor de cada posição
    {
        for(int y = 1; y<=x; y++)
        {
            int num;
            cout<<"N"<<char(163)<<"mero da "<<x<<char(166)<<" linha e "<<y<<char(166)<<" posi"<<char(135)<<char(198)<<"o:";
            cin>>num;
            p.add(num,x,y);
        }
    }

    int sum = p.calculate();        //calcula o caminho mínimo

    cout<<"Menor soma: "<<sum<<endl;
}
