#ifndef NODE_H
#define NODE_H
#include<cstdlib>
template<typename T>
class Node {
private:
    T _value;
    Node<T> *_next = nullptr;

public:
    Node(T value)
    {
        _value = value;
    }

    ~Node()
    {
        if (_next != NULL)
        {
            delete _next;
            _next = NULL;
        }
    }

    T getValue()
    {
        return _value;
    }

    Node *getNext()
    {
        return _next;
    }

    void setNext(Node *next)
    {
        _next = next;
    }

    void setValue(T value)
    {
        _value = value;
    }

    Node *append(T nextValue)
    {
        return _next = new Node(nextValue);
    }
    bool haveNode()
    {
        return _next!=nullptr;
    }

};


#endif // NODE_H
