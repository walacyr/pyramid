#include "NumericPyramid.h"

NumericPyramid::NumericPyramid(int height)
{
    _height = height;
    for(int x = 1; x<=height; x++)
    {
        vector<int>* row = new vector<int>(x);      //Criei vetores para o alocamento de cada linha
        pyramid.append(row);                        //Salvei o endereço deles numa lista encadeada que eu já havia implementado anteriormente na faculdade
    }
}
void NumericPyramid::add(int num, int row, int position)
{
    pyramid.get(row)->at(position-1) = num;         //Adicionar o número pela linha e posição
}

int NumericPyramid::calculate()
{
    int sum = 0;
    for(int x = 1; x <= _height; x++)               //percorre as linhas
    {
        int min = pyramid.get(x)->at(0);
        for(int y = 0; y< x; y++)                   //percorre as posições de cada linha e compara o menor
        {
            if(pyramid.get(x)->at(y) < min)
            {
                min = pyramid.get(x)->at(y);
            }
        }
        sum += min;                                 //soma os mínimos
    }

    return sum;
}
