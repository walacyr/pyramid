#ifndef NUMERICPYRAMID_H
#define NUMERICPYRAMID_H
#include <vector>
#include "List.h"
#include <string>
using namespace std;


class NumericPyramid
{
private:
    int _height;
    int lowersum = 0;
    int row = 0, position = 0;
    List<vector<int>*> pyramid;

public:
    NumericPyramid(int height);
    void add(int num, int row, int position);
    int calculate();

};

#endif // NUMERICPYRAMID_H
